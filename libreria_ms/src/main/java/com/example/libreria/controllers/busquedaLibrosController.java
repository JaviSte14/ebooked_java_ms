package com.example.libreria.controllers;
    import com.example.libreria.exceptions.bookNotFoundException;
    import com.example.libreria.models.busquedaLibros;
    import com.example.libreria.repositories.busquedaLibrosRepository;
    import org.springframework.web.bind.annotation.*;

@RestController
public class busquedaLibrosController {
    private final busquedaLibrosRepository BusquedaLibrosRepository;

    public busquedaLibrosController(busquedaLibrosRepository busquedaLibrosRepository) {
        BusquedaLibrosRepository = busquedaLibrosRepository;
    }
    @GetMapping ("/busquedaLibros/{bookname}")
    busquedaLibros getbusquedaLibros(@PathVariable String bookname){
        return BusquedaLibrosRepository.findById(bookname).
                orElseThrow(() -> new bookNotFoundException(("No se encontro libro con el nombre : " +
                        bookname)));
    }
@PostMapping("/busquedaLibros")
    busquedaLibros newbusquedaLibros (@RequestBody busquedaLibros libros) {
        return BusquedaLibrosRepository.save(libros);
    }
}
