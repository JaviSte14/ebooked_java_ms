package com.example.libreria.exceptions;

public class NobookException extends RuntimeException {
    public NobookException(String message) {
        super(message);
    }
}
