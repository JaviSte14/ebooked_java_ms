package com.example.libreria.exceptions;

public class bookNotFoundException extends RuntimeException {
    public bookNotFoundException(String message) {
        super(message);
    }
}
