package com.example.libreria.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
@ControllerAdvice
@ResponseBody
public class NobookAdvice {
    @ResponseBody
    @ExceptionHandler(NobookException.class)
    @ResponseStatus(HttpStatus.CONFLICT)
    String NobookAdvice(NobookException ex){
        return ex.getMessage();
    }
}
