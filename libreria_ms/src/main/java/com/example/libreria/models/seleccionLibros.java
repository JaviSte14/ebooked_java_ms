package com.example.libreria.models;

import org.springframework.data.annotation.Id;

public class seleccionLibros {
    @Id
    private String bookname;
    private Integer cantidad;
    private float precio;
    private float precio_total;

    public seleccionLibros(String bookname, Integer cantidad, float precio, float precio_total) {
        this.bookname = bookname;
        this.cantidad = cantidad;
        this.precio = precio;
        this.precio_total = precio_total;
    }

    public String getBookname() {
        return bookname;
    }

    public void setBookname(String bookname) {
        this.bookname = bookname;
    }

    public Integer getCantidad() {
        return cantidad;
    }

    public void setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
    }

    public float getPrecio() {
        return precio;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }

    public float getPrecio_total() {
        return precio_total;
    }

    public void setPrecio_total(float precio_total) {
        this.precio_total = precio_total;
    }
}
