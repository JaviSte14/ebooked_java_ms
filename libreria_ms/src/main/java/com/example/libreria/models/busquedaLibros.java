package com.example.libreria.models;

import org.springframework.data.annotation.Id;

public class busquedaLibros {
    @Id
    private String bookname;
    private String author;
    private String booktype;

    public busquedaLibros(String bookname, String author, String booktype) {
        this.bookname = bookname;
        this.author = author;
        this.booktype = booktype;
    }

    public String getBookname() {
        return bookname;
    }

    public void setBookname(String bookname) {
        this.bookname = bookname;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getBooktype() {
        return booktype;
    }

    public void setBooktype(String booktype) {
        this.booktype = booktype;
    }
}
