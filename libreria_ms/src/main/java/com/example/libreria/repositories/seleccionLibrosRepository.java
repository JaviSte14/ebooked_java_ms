package com.example.libreria.repositories;
import com.example.libreria.models.seleccionLibros;
import org.springframework.data.mongodb.repository.MongoRepository;
import java.util.List;
public interface seleccionLibrosRepository extends MongoRepository<seleccionLibros, String> {
}
