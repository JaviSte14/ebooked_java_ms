package com.example.libreria.repositories;

import com.example.libreria.models.busquedaLibros;
import com.example.libreria.models.seleccionLibros;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface busquedaLibrosRepository extends MongoRepository <busquedaLibros, String> {
    List<seleccionLibros> findBybookname(String bookname);
}
