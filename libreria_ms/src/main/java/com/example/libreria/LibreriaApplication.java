package com.example.libreria;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class LibreriaApplication {

	public static void main(String[] args) {
		ConfigurableApplicationContext run = SpringApplication.run(LibreriaApplication.class, args);
	}


}
